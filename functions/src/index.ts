import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';

admin.initializeApp();
const Collection = 'babySister';


exports.updateBabySister = functions.https.onRequest(async (request, response) => {
  try {
    const { data } = request.body;
    const { documentReference, ...dataToUpdate } = data;

    await admin
      .firestore()
      .collection(Collection)
      .doc(documentReference)
      .update(dataToUpdate);
    
    response.status(200).send('Updated');

  } catch (error) {
    console.log(error);
    response.status(500).send(error);
  }
});

exports.addBabySister = functions.https.onRequest(async (request, response) => {
  try {
    const { data } = request.body;
    const dataToSave = { ...data, votes: 0, numeros: [] };

    await admin
      .firestore()
      .collection(Collection)
      .add(dataToSave);
    
    response.status(200).send('Created');

    } catch (error) {
    console.log(error);
    response.status(500).send(error);
  }
});

exports.removeBabySister = functions.https.onRequest(async (request, response) => {
  try {
    const { data } = request.body;
    const { documentReference } = data;

    const table = admin.firestore().collection(Collection).doc(documentReference);
    await table.delete();

    response.status(200).send('Removed');
  } catch (error) {
    console.log(error);
    response.status(500).send(error);
  }
});
