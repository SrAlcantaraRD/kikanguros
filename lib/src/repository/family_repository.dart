import 'package:cloud_firestore/cloud_firestore.dart';

class FamilyRepository {
  CollectionReference _firestore = Firestore.instance.collection("babySister");

  Stream<QuerySnapshot> fetchAllFamilies() {
    return _firestore.snapshots();
  }
}
