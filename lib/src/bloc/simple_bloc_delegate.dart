import 'package:bloc/bloc.dart';


/// All we're doing in this case is printing all state changes (transitions) and errors to the console just
/// so that we can see what's going on when we're running our app. You can hook up your BlocDelegate to google
/// analytics, sentry, crashlytics, etc...
class SimpleBlocDelegate extends BlocDelegate {
  @override
  void onEvent(Bloc bloc, Object event) {
    super.onEvent(bloc, event);
    print(event);
  }

  @override
  void onError(Bloc bloc, Object error, StackTrace stacktrace) {
    super.onError(bloc, error, stacktrace);
    print(error);
  }

  @override
  void onTransition(Bloc bloc, Transition transition) {
    super.onTransition(bloc, transition);
    print(transition);
  }
}
