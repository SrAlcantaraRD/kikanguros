import 'package:flutter/material.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kikanguros/src/screens/babySitting/new/new.dart';
import 'package:kikanguros/src/screens/home/home_screen.dart';
import 'package:kikanguros/src/screens/login/login.dart';

import 'bloc/authentication/bloc.dart';

// Esto es interesante para saber como construir nuevas paginas con un BLoC
// https://medium.freecodecamp.org/using-streams-blocs-and-sqlite-in-flutter-2e59e1f7cdce

//     void _navigateToNote(Note note) async {
//         // Push ViewNotePage, and store any return value in update. This will
//         // be used to tell this page to update the note stream after a note is deleted.
//         // If a note isn't deleted, this will be set to null and the note stream will
//         // not be updated.
//         bool update = await Navigator.of(context).push(
//             MaterialPageRoute(
//                 // Once again, use the BlocProvider to pass the ViewNoteBloc
//                 // to the ViewNotePage
//                 builder: (context) => BlocProvider(
//                     bloc: ViewNoteBloc(),
//                     child: ViewNotePage(
//                         note: note,
//                     ),
//                 ),
//             ),
//         );

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings route) {
    switch (route.name) {
      case '/':
        return handleLoginStatus();
      case '/createBabySitting':
        return MaterialPageRoute(
          builder: (context) => CreateBabySittingScreen(),
        );

      // case '/familyPage':
      //   // return PageRouteBuilder(
      //   //   transitionDuration: Duration(milliseconds: 700),
      //   //   pageBuilder: (context, _, __) => BlocProvider(
      //   //       bloc: FamilyBloc(),
      //   //       child: FamilyScreen(family: route.arguments as Family),
      //   //     ),
      //   // );
      // return MaterialPageRoute(
      //   builder: (context) => BlocProvider(
      //         bloc: FamilyBloc(),
      //         child: FamilyScreen(family: route.arguments as Family),
      //       ),
      // );

      default:
        return routeErrorScreen(route.name);
    }
  }

  static Route<dynamic> handleLoginStatus() {
    return MaterialPageRoute(
      builder: (BuildContext context) => BlocBuilder(
            bloc: BlocProvider.of<AuthenticationBloc>(context),
            builder: (BuildContext context, AuthenticationState state) {
              if (state is Uninitialized) {
                return Scaffold(
                  body: Container(
                    alignment: Alignment.center,
                    child: FlareActor(
                      "assets/Teddy.flr",
                      alignment: Alignment.center,
                      fit: BoxFit.contain,
                      animation: "idle",
                    ),
                  ),
                );
              }
              if (state is Unauthenticated) {
                return LoginScreen();
              }
              if (state is Authenticated) {
                return HomeScreen();
              }
            },
          ),
    );
  }

  static Route<dynamic> routeErrorScreen(String routeName) {
    print(routeName);
    return MaterialPageRoute(builder: (BuildContext context) {
      return Scaffold(
          appBar: AppBar(title: Text('PAGE ERROR')),
          // TODO: El contenido debería de ser un gif.
          // TODO: https://www.2dimensions.com/a/kautuk/files/flare/bob-minion/preview
          // TODO: https://www.2dimensions.com/a/nastya/files/flare/under-construction/preview
          // TODO: https://www.2dimensions.com/a/JuanCarlos/files/nima/apes
          // TODO: https://www.2dimensions.com/a/ginger/files/nima/low-wolf
          body: ListView(
            children: <Widget>[
              Center(
                child: FlareActor(
                  "assets/Teddy.flr",
                  alignment: Alignment.center,
                  fit: BoxFit.contain,
                  animation: "idle",
                ),
              ),
              Center(
                child: Text('No route defined for $routeName'),
              ),
            ],
          ));
    });
  }
}
