import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:cloud_functions/cloud_functions.dart';
import 'package:flutter/widgets.dart';

class Family {
  final String _name;
  // final String key;
  int _votes = 1;
  final String collectionName = "babySister";
  final String urlToImage;
  static final List<String> urlImages = [
    "assets/images/spelt_noodles.png",
    "assets/images/spelt_italian.png",
    "assets/images/spelt_spaghetti.png",
    "assets/images/spelt_tagliatelle.png",
    "assets/images/spelt_penne.png",
    "assets/images/spelt_fusilli.png",
  ];

  final List<dynamic> numeros;
  final DocumentReference reference;

  String getName() => _name;
  int getVotes() => _votes;

  Family.fromFirestoreMap(Map<String, dynamic> map, {this.reference})
      : assert(map['name'] != null),
        assert(map['votes'] != null),
        assert(map['numeros'] != null),
        _name = map['name'],
        _votes = map['votes'],
        numeros = map['numeros'],
        urlToImage = urlImages[map['votes']];

  Family.fromSnapshot(DocumentSnapshot snapshot)
      : this.fromFirestoreMap(snapshot.data, reference: snapshot.reference);

  Object toObject() {
    return {
      'name': getName(),
      'votes': getVotes(),
      'documentReference': reference.documentID,
    };
  }

  void incVotes() {
    this._votes = getVotes() + 1;
  }

  Future<void> increaseVotes() async {
    try {
      incVotes();

      await CloudFunctions.instance
          .call(functionName: "updateBabySister", parameters: toObject());
    } catch (error) {
      debugPrint(error);
    }
  }

  Future<void> delete() async {
    try {
      await CloudFunctions.instance
          .call(functionName: "removeBabySister", parameters: toObject());
    } catch (error) {
      debugPrint(error);
    }
  }

  Future<void> create() async {
    try {
      await CloudFunctions.instance
          .call(functionName: "add$collectionName", parameters: toObject());
    } catch (error) {
      debugPrint(error);
    }
  }

  @override
  String toString() => "Family<$getName():$getVotes()> --- $numeros";

  String getDesc() => "When we yield a state in the private mapEventToState handlers, we are always yielding a new state ";
}
