import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

@immutable
abstract class FamilyEvent extends Equatable {
  FamilyEvent([List props = const []]) : super(props);
}

class KLKChanged extends FamilyEvent {
  //TODO: Esto debería de ser la clase Familia
  final String strKLK;

  KLKChanged({@required this.strKLK}) : super([strKLK]);

  @override
  String toString() => 'KLKChanged { strKLK :$strKLK }';
}