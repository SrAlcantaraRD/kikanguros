import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kikanguros/src/bloc/authentication/authentication_bloc.dart';
import 'package:kikanguros/src/bloc/login/bloc.dart';
import 'package:kikanguros/src/repository/user_repository.dart';
import 'package:kikanguros/src/screens/login/login.dart';

class LoginScreen extends StatefulWidget {
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  LoginBloc _loginBloc;
  UserRepository _userRepository;

  @override
  void initState() {
    super.initState();
    _userRepository =  BlocProvider.of<AuthenticationBloc>(context)
                              .userRepository;
    _loginBloc = LoginBloc(
      userRepository: _userRepository,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Login')),
      body: BlocProvider<LoginBloc>(
        bloc: _loginBloc,
        child: LoginForm(userRepository: _userRepository),
      ),
    );
  }

  @override
  void dispose() {
    _loginBloc.dispose();
    super.dispose();
  }
}
