class StringConstant {
  // Bottom bar items
  static const String profile = "Perfil";
  static const String openEvents = "Eventos";
  static const String assignedEvents = "Asignados";
  static const String seeEventsActive = "Historial";
  static const String createEvent = "Crear evento";

  // Baby Sitting
  static const String babySittingCreate = "Crear";
  static const String creatioFailure = "Error al crear";
  static const String creatioInProcess = "Creando...";
}
