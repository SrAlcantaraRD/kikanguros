import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:kikanguros/src/models/models.dart';
import 'package:rxdart/rxdart.dart';
import 'babySitting_repository.dart';
import 'bloc.dart';

class BabySittingBloc extends Bloc<BabySittingEvent, BabySittingState> {
  final _repository = BabySittingRepository();
  final PublishSubject<Event> _publishSubjectOrder =
      new PublishSubject<Event>();

  @override
  BabySittingState get initialState => BabySittingState.empty();

  @override
  Stream<BabySittingState> mapEventToState(BabySittingEvent event) async* {
    if (event is BabySittingLoaded) {
      yield* _mapLoadingToState();
    }
  }

  Observable<Event> get observableLastOrder => _publishSubjectOrder.stream;

  Stream<BabySittingState> _mapLoadingToState() async* {
    yield BabySittingState.loading();
  }

  Stream<QuerySnapshot> getAllBabySitting() {
    return _repository.fetchAllSitting();
  }

  @override
  dispose() {
    _publishSubjectOrder.close();
    super.dispose();
  }
}
