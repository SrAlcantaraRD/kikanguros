import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

@immutable
abstract class BabySittingEvent extends Equatable {
  BabySittingEvent([List props = const []]) : super(props);
}

class BabySittingLoaded extends BabySittingEvent {
  final String documentId;

  BabySittingLoaded({@required this.documentId}) : super([documentId]);

  @override
  String toString() => 'BabySittingLoaded { documentId: $documentId }';
}


class BabySittingCreated extends BabySittingEvent {
  @override
  String toString() => 'BabySittingCreated';
}
