import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kikanguros/src/screens/babySitting/bloc/bloc.dart';
import 'package:kikanguros/src/translate/translate.dart';

import 'new.dart';

class CreateBabySittingScreen extends StatefulWidget {
  CreateBabySittingScreen({Key key}) : super(key: key);

  State<CreateBabySittingScreen> createState() =>
      _CreateBabySittingScreenState();
}

class _CreateBabySittingScreenState extends State<CreateBabySittingScreen> {
  BabySittingBloc _bloc;

  @override
  void initState() {
    super.initState();
    _bloc = new BabySittingBloc();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: BlocProvider<BabySittingBloc>(
          bloc: _bloc,
          child: NewBabySittingForm(),
        ),
      ),
    );
  }

  @override
  void dispose() {
    _bloc.dispose();
    super.dispose();
  }
}
