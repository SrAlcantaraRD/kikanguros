import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:kikanguros/src/models/models.dart';
import 'package:kikanguros/src/repository/repositoy.dart';
import 'package:rxdart/rxdart.dart';

import 'bloc.dart';

class FamilyBloc extends Bloc<FamilyEvent, FamilyState> {
  final _repository = FamilyRepository();
  final PublishSubject<Family> _publishSubjectOrder =
      new PublishSubject<Family>();

  @override
  FamilyState get initialState => FamilyState.empty();

  @override
  Stream<FamilyState> mapEventToState(FamilyEvent event) async* {
    if (event is KLKChanged) {
      yield* _mapKLKChangedToState(event.strKLK);
    }
  }

  Observable<Family> get observableLastOrder => _publishSubjectOrder.stream;

  Stream<FamilyState> _mapKLKChangedToState(String strKLK) async* {
    yield currentState.setKLK(strKLK: strKLK);
  }

  Stream<QuerySnapshot> getAllFamilies() {
    return _repository.fetchAllFamilies();
  }

  @override
  dispose() {
    _publishSubjectOrder.close();
    super.dispose();
  }
}
