import 'package:meta/meta.dart';

@immutable
class FamilyState {
  final String strKLK;

  FamilyState({
    @required this.strKLK,
  });

  String get getKLK => strKLK;

  @override
  String toString() {
    return '''FamilyState {
      strKLK: $strKLK,
    }''';
  }

  factory FamilyState.empty() {
    return FamilyState(
      strKLK: '',
    );
  }

  FamilyState setKLK({
    String strKLK,
  }) {
    return copyWith(
      strKLK: strKLK,
    );
  }

  FamilyState copyWith({
    String strKLK,
  }) {
    return FamilyState(
      strKLK: strKLK ?? this.strKLK,
    );
  }
}
