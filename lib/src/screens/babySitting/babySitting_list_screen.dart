import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:kikanguros/src/bloc/family/bloc.dart';
import 'package:kikanguros/src/components/dart.dart';
import 'package:kikanguros/src/models/models.dart';
import 'package:animated_floatactionbuttons/animated_floatactionbuttons.dart';

import 'bloc/bloc.dart';
import 'new/new.dart';

class BabySittingListScreen extends StatefulWidget {
  State<BabySittingListScreen> createState() => _BabySittingListScreen();
}

class _BabySittingListScreen extends State<BabySittingListScreen> {
  BabySittingBloc _bloc;

  @override
  void initState() {
    _bloc = new BabySittingBloc();
    super.initState();
  }

  Widget float1() {
    return Container(
      child: FloatingActionButton(
        onPressed: null,
        tooltip: 'First button',
        heroTag: "Inbox",
        child: Icon(Icons.filter_list),
      ),
    );
  }

  Widget floatingButtonAdd(BuildContext context) {
    return Container(
      child: FloatingActionButton(
        onPressed: () {
          Navigator.pushNamed(context, '/createBabySitting');
        },
        heroTag: "add-babySitting",
        child: Icon(Icons.add),
      ),
    );
  }

  Widget _buildList(BuildContext context, List<DocumentSnapshot> snapshot) {
    double childAspectRatio = MediaQuery.of(context).size.width /
        (MediaQuery.of(context).size.height / 1.0);

    return Scaffold(
      floatingActionButton: AnimatedFloatingActionButton(
        //Fab list
        fabButtons: <Widget>[float1(), floatingButtonAdd(context)],
        colorStartAnimation: Colors.blue,
        colorEndAnimation: Colors.red,
        animatedIconData: AnimatedIcons.menu_close, //To principal button
      ),
      body: new Container(
        child: new PhysicalModel(
          color: Colors.transparent,
          clipBehavior: Clip.antiAlias,
          child: new GridView.builder(
            itemCount: snapshot.length,
            gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
              childAspectRatio: childAspectRatio,
            ),
            itemBuilder: (BuildContext context, int index) {
              Family family = Family.fromSnapshot(snapshot[index]);
              return FamilyWidget(family: family);
            },
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment(0.0, 0.0),
      child: StreamBuilder<QuerySnapshot>(
        stream: _bloc.getAllBabySitting(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          print(snapshot.connectionState);
          if (snapshot.hasError) {
            return new Text('Error: ${snapshot.error}');
          }
          switch (snapshot.connectionState) {
            case ConnectionState.waiting:
              return new Center(child: new CircularProgressIndicator());
            default:
              return _buildList(context, snapshot.data.documents);
          }
        },
      ),
    );
  }

  @override
  void dispose() {
    _bloc.dispose();
    super.dispose();
  }
}
