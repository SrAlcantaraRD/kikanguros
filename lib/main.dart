import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:kikanguros/src/bloc/simple_bloc_delegate.dart';
import 'src/app.dart';
import 'package:intl/date_symbol_data_local.dart';

void main() {
  BlocSupervisor().delegate = SimpleBlocDelegate();
  initializeDateFormatting("es", null);
  runApp(App());
}


