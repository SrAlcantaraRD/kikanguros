import 'package:flutter/material.dart';
import 'package:kikanguros/src/bloc/icon/bloc.dart';
import 'package:kikanguros/src/screens/babySitting/dart.dart';
import 'package:kikanguros/src/screens/profile/dart.dart';

class HomeScreen extends StatefulWidget {
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int _selectedIndex = 0;
  IconsBloc _iconsBloc = new IconsBloc();

  Widget _buttonNavigationBar(BuildContext context) {
    _iconsBloc.notifyFavoriteIcon(true);
    _iconsBloc.notifyProfileIcon(true);

    return BottomNavigationBar(
      items: <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: Icon(Icons.search),
          title: Text('Canguros'),
        ),
        BottomNavigationBarItem(
          icon: new Stack(
            children: <Widget>[
              new Icon(Icons.favorite),
              new StreamBuilder(
                stream: _iconsBloc.favoritesNotification,
                initialData: false,
                builder: (context, AsyncSnapshot snapshot) {
                  if (!snapshot.data) return Container(height: 0, width: 0);
                  return _badge();
                },
              )
            ],
          ),
          title: Text('Favoritos'),
        ),
        BottomNavigationBarItem(
          icon: new Stack(
            children: <Widget>[
              new Icon(Icons.local_florist),
              new StreamBuilder(
                stream: _iconsBloc.cartNotification,
                initialData: false,
                builder: (context, AsyncSnapshot snapshot) {
                  if (!snapshot.data) return Container(height: 0, width: 0);
                  return _badge();
                },
              )
            ],
          ),
          title: new Text("Canguros"),
        ),
        BottomNavigationBarItem(
          icon: new Stack(
            children: <Widget>[
              new Icon(Icons.person),
              new StreamBuilder(
                stream: _iconsBloc.profileNotification,
                initialData: false,
                builder: (context, AsyncSnapshot snapshot) {
                  if (!snapshot.data) return Container(height: 0, width: 0);
                  return _badge();
                },
              )
            ],
          ),
          title: Text('Perfil'),
        ),
      ],
      currentIndex: _selectedIndex,
      fixedColor: Theme.of(context).primaryColor,
      onTap: _onItemTapped,
      type: BottomNavigationBarType.fixed,
    );
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  Widget _badge() {
    return new Positioned(
      top: 0.0,
      right: 0.0,
      child: new Icon(
        Icons.brightness_1,
        size: 8.0,
        color: Colors.amber,
      ),
    );
  }

  final _widgetOptions = [
    new BabySittingListScreen(),
    new Center(child: new Text('Mis canguros|familias favoritos')),
    new Center(child: new Text('TO DO')),
    new ProfileScreen(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: _buttonNavigationBar(context),
      body: _widgetOptions.elementAt(_selectedIndex),
    );
  }

  @override
  void dispose() {
    _iconsBloc.dispose();
    super.dispose();
  }
}
