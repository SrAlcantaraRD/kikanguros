import 'package:cloud_firestore/cloud_firestore.dart';

class BabySittingRepository {
  // CollectionReference _firestore = Firestore.instance.collection("babySitting");
  CollectionReference _firestore = Firestore.instance.collection("babySister");

  

  Stream<QuerySnapshot> fetchAllSitting() {
    return _firestore.snapshots();
  }
}
