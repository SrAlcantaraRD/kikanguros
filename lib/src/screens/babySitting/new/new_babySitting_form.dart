import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kikanguros/src/screens/babySitting/bloc/bloc.dart';
import 'package:kikanguros/src/translate/translate.dart';

class NewBabySittingForm extends StatefulWidget {
  State<NewBabySittingForm> createState() => _NewBabySittingFormState();
}

class _NewBabySittingFormState extends State<NewBabySittingForm> {
  DateTime date;
  BabySittingBloc _bloc;

  String _valueDate;
  String _valueTime;

  @override
  void initState() {
    super.initState();
    _bloc = BlocProvider.of<BabySittingBloc>(context);
    _valueDate = DateFormat("yMMMMEEEEd", "es").format(DateTime.now());
    _valueTime = DateFormat.Hm().format(DateTime.now());
  }

  @override
  Widget build(BuildContext context) {
    Widget buttonSection = Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              _buildButtonColumn(
                  Colors.red, Icons.close, "add-babySitting", _cancellCreaction),
              _buildButtonColumn(null, Icons.add, "", null),
            ],
          ),
        )
      ],
    );
    Widget formSection = Container(
      margin: EdgeInsets.only(top: 60),
      child: Column(
        children: [
          Container(
            width: double.maxFinite,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  flex: 3,
                  child: FlatButton(
                    color: Colors.white,
                    child: Text(
                      _valueDate,
                      textAlign: TextAlign.right,
                      style: TextStyle(
                        fontSize: 16,
                        letterSpacing: 1,
                        fontStyle: FontStyle.italic,
                      ),
                    ),
                    onPressed: _selectDate,
                  ),
                ),
                Expanded(
                  child: FlatButton(
                    color: Colors.white,
                    child: Text(
                      _valueTime,
                      style: TextStyle(
                        fontSize: 16,
                        letterSpacing: 1,
                        fontStyle: FontStyle.italic,
                      ),
                    ),
                    onPressed: _selectTime,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
    Widget titleSection = Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Container(
          padding: EdgeInsets.only(top: 20),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: Text(
                  "Crear Canguro",
                  style: TextStyle(
                    fontSize: 24,
                    letterSpacing: 1,
                    fontStyle: FontStyle.italic,
                  ),
                ),
              ),
            ],
          ),
        )
      ],
    );

    return BlocListener(
      bloc: _bloc,
      listener: _enableListeners,
      child: Container(
        // color: Theme.of(context).primaryColor,
        child: Padding(
          padding: EdgeInsets.all(30),
          child: Stack(
            children: [
              titleSection,
              formSection,
              buttonSection,
            ],
          ),
        ),
      ),
    );
  }

  void _cancellCreaction() {
    Navigator.of(context).pop();
  }

  Column _buildButtonColumn(
      Color _color, IconData _icon, String _heroTag, VoidCallback _onPress) {
    return new Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          margin: const EdgeInsets.only(top: 8),
          child: FittedBox(
            child: FloatingActionButton(
              heroTag: _heroTag,
              child: Icon(_icon),
              onPressed: _onPress,
              backgroundColor: _color,
            ),
          ),
        ),
      ],
    );
  }

  Future _selectDate() async {
    DateTime _now = new DateTime.now();
    DateTime picked = await showDatePicker(
      context: context,
      initialDate: _now,
      firstDate: _now,
      lastDate: _now.add(new Duration(days: 365)),
    );

    if (picked != null) {
      setState(
          () => _valueDate = DateFormat("yMMMMEEEEd", "es").format(picked));
    }
  }

  Future _selectTime() async {
    DateTime _now = new DateTime.now();
    TimeOfDay picked = await showTimePicker(
      context: context,
      initialTime: TimeOfDay.now(),
      builder: (BuildContext context, Widget child) {
        return MediaQuery(
          data: MediaQuery.of(context).copyWith(alwaysUse24HourFormat: true),
          child: child,
        );
      },
    );
    if (picked != null) {
      setState(
        () => _valueTime = DateFormat.Hm().format(
              DateTime(
                _now.year,
                _now.month,
                _now.day,
                picked.hour,
                picked.minute,
              ),
            ),
      );
    }
  }

  void _enableListeners(BuildContext context, BabySittingState state) {
    if (state.isSubmitting) {
      Scaffold.of(context)
        ..hideCurrentSnackBar()
        ..showSnackBar(
          SnackBar(
            content: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(StringConstant.creatioInProcess),
                CircularProgressIndicator(),
              ],
            ),
          ),
        );
    }
    if (state.isSuccess) {
      _bloc.dispatch(BabySittingCreated());
      Navigator.of(context).pop();
    }
    if (state.isFailure) {
      Scaffold.of(context)
        ..hideCurrentSnackBar()
        ..showSnackBar(
          SnackBar(
            content: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(StringConstant.creatioFailure),
                Icon(Icons.error),
              ],
            ),
            backgroundColor: Colors.red,
          ),
        );
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  // void _onStartDateChange() {
  //   _bloc.dispatch(
  //     EmailChanged(email: _emailController.text),
  //   );
  // }

  // void _onFinishDateChange() {
  //   _bloc.dispatch(
  //     EmailChanged(email: _emailController.text),
  //   );
  // }

}
