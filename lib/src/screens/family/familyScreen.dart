import 'package:flutter/material.dart';
import 'package:kikanguros/src/bloc/family/family_bloc.dart';
import 'package:kikanguros/src/models/models.dart';

class FamilyScreen extends StatefulWidget {
  final Family family;
  FamilyScreen({Key key, this.family}) : super(key: key);

  @override
  _FamilyScreen createState() => new _FamilyScreen();
}

class _FamilyScreen extends State<FamilyScreen> {
  final FamilyBloc _cartBloc = new FamilyBloc();
 
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: Colors.white,
      appBar: new AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: new SafeArea(
        child: new Column(
          children: <Widget>[
            new Container(
              padding: EdgeInsets.symmetric(horizontal: 20),
              height: MediaQuery.of(context).size.height * 0.73,
              child: new SingleChildScrollView(
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new Center(
                      child: new StreamBuilder(
                        initialData: null,
                        stream: _cartBloc.observableLastOrder,
                        builder: (context, AsyncSnapshot<Family> snapshot) {
                          return new Hero(
                            tag: "tagHero${widget.family.reference.documentID}",
                            child: new Image.asset(
                              widget.family.urlToImage,
                              fit: BoxFit.cover,
                              height: MediaQuery.of(context).size.height * 0.4,
                            ),
                          );
                        },
                      ),
                    ),
                    new Container(
                      margin: EdgeInsets.only(top: 20),
                      child: new Text(
                        widget.family.getName(),
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 40,
                          color: Colors.black,
                        ),
                      ),
                    ),
                    new Container(
                      margin: EdgeInsets.only(top: 10),
                      child: new Text(
                        "${widget.family.getVotes()}g",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 20,
                          color: Colors.grey,
                        ),
                      ),
                    ),
                    new Container(
                      margin: EdgeInsets.only(top: 40, bottom: 40),
                      child: new Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          new Text(
                            "About the product:",
                            style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 20,
                            ),
                          ),
                          new Padding(
                            padding: EdgeInsets.only(top: 10),
                            child: new Text(
                              widget.family.getDesc(),
                              style: TextStyle(
                                color: Colors.grey,
                                fontSize: 18,
                              ),
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
            new Container(
              decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    color: Colors.white,
                    blurRadius: 30.0, // has the effect of softening the shadow
                    spreadRadius: 5.0, // has the effect of extending the shadow
                    offset: Offset(
                      0.0, // horizontal, move right 10
                      -20.0, // vertical, move down 10
                    ),
                  )
                ],
              ),
              padding: EdgeInsets.symmetric(horizontal: 20),
              height: MediaQuery.of(context).size.height * 0.1,
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  new FlatButton.icon(
                    onPressed: () {},
                    icon: new Icon(Icons.favorite_border),
                    label: new Text(""),
                  ),
                  new SizedBox(
                    width: MediaQuery.of(context).size.width * 0.6,
                    child: new RaisedButton(
                      color: Colors.amber,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(60),
                      ),
                      padding: EdgeInsets.all(20),
                      onPressed: () {
                        // _cartBloc.addOrderToCart(widget.product, _quantity);
                        Navigator.of(context).pop();
                      },
                      child: new Text(
                        "Add to cart",
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
