import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kikanguros/src/bloc/authentication/bloc.dart';

class ProfileScreen extends StatefulWidget {
  State<ProfileScreen> createState() => _ProfileScreen();
}

class _ProfileScreen extends State<ProfileScreen> {
  void _signOut(BuildContext context) async {
    try {
      BlocProvider.of<AuthenticationBloc>(context).dispatch(
        LoggedOut(),
      );
    } catch (e) {
      print(e);
    }
  }

  // CloudFunctions.instance.call(
  //   functionName: "addBabySister",
  //   parameters: {"name": faker.person.name()},
  // );

  @override
  Widget build(BuildContext context) {
    return new AppBar(
      title: Text('Home'),
      actions: <Widget>[
        new IconButton(
          icon: Icon(Icons.exit_to_app),
          onPressed: () => _signOut(context),
        )
      ],
    );
  }
}
