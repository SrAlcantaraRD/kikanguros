import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kikanguros/src/bloc/family/bloc.dart';
import 'package:kikanguros/src/models/models.dart';
import 'package:kikanguros/src/screens/family/familyScreen.dart';

class FamilyWidget extends StatelessWidget {
  final Family family;

  FamilyWidget({Key key, this.family}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height / 1.2;
    double fontSize = (height / 24).round().toDouble();

    return new GestureDetector(
      onTap: () {
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) => new FamilyScreen(family: this.family),
          ),
        );
      },
      child: new Container(
        padding: EdgeInsets.fromLTRB(20, 20, 20, 5),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(
            Radius.circular(10),
          ),
        ),
        child: new Column(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            new Center(
              child: new Hero(
                tag: "tagHero${this.family.reference.documentID}",
                child: new Image.asset(
                  this.family.urlToImage,
                  fit: BoxFit.cover,
                  height: height * 0.20,
                ),
              ),
            ),
            new Container(
              height: height * 0.25,
              margin: EdgeInsets.only(top: 10),
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new Text("\$${this.family.getVotes()}",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.black,
                          fontSize: fontSize)),
                  new Container(
                      margin: EdgeInsets.only(top: 20, bottom: 10),
                      child: new Text("${this.family.getName()}",
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.black,
                            fontSize: fontSize * 0.65,
                          ))),
                  new Text("${this.family.getName()}g",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.grey,
                        fontSize: fontSize * 0.48,
                      ))
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
