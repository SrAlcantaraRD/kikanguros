import 'package:meta/meta.dart';

@immutable
class BabySittingState {
  final bool isLoading;
  final bool isSubmitting;
  final bool isSuccess;
  final bool isFailure;

  BabySittingState({
    @required this.isLoading,
    @required this.isSubmitting,
    @required this.isSuccess,
    @required this.isFailure,
  });

  @override
  String toString() {
    return '''BabySittingState {
      isLoading: $isLoading,
    }''';
  }

  factory BabySittingState.empty() {
    return BabySittingState(
      isLoading: false,
      isSubmitting: false,
      isSuccess: false,
      isFailure: false,
    );
  }

  factory BabySittingState.loading() {
    return BabySittingState(
      isLoading: true,
      isSubmitting: false,
      isSuccess: false,
      isFailure: false,
    );
  }

  factory BabySittingState.submitting() {
    return BabySittingState(
      isLoading: false,
      isSubmitting: true,
      isSuccess: false,
      isFailure: false,
    );
  }

  factory BabySittingState.success() {
    return BabySittingState(
      isLoading: false,
      isSubmitting: false,
      isSuccess: true,
      isFailure: false,
    );
  }

  factory BabySittingState.failure() {
    return BabySittingState(
      isLoading: false,
      isSubmitting: false,
      isSuccess: false,
      isFailure: true,
    );
  }

}
